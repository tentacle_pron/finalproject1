
<%@ page  language="java"  %>
<%@ include file="taglib.jsp" %>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<html>
<head>
    <title>SignIn Page</title>
</head>
<body>
<form class="form-signin" name="f" role="form" action="login" method="POST">
    <h2 class="form-signin-heading">Please sign in</h2>
    <input type="text" name="username" class="form-control" placeholder="Name" required autofocus>
    <input type="password" name="password" class="form-control" placeholder="Password" required>
    <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Sign in</button>
</form>
<a class="btn btn-default" href="/register" role="button">New user?</a>

</body>
</html>
