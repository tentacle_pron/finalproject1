<%--
  Created by IntelliJ IDEA.
  User: felix
  Date: 6/18/19
  Time: 11:32 AM
  To change this template use File | Settings | File Templates.
--%>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%@ page language="java" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <title>My Profile</title>
</head>
<body>

<div class="col-sm-3">

        <li class="list-group-item text-muted" contenteditable="false">Profile</li>
        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Name: </strong></span> ${user.username}</li>

    </ul>
</div>
<div class="panel panel-default">
<form method="get" action="/user/wishlist/${user.wishlist.id}">
    <button class="btn btn-default" type="submit">my Wishlist</button>

    </div>
</form>
    <form method="get" action="/user/cart/${user.cart.id}">
        <button class="btn btn-default" type="submit">My Cart</button>

        </div>
    </form>
</div>


</body>
</html>
