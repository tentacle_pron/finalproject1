<%@ page  language="java"  %>
<%@include file="taglib.jsp"%>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<html>
<head>
    <title>Home Page</title>
</head>
<body><a href="/register">Registration</a>
<a href="/login">Login</a>
<a href="/admin/add">Add a new product</a>
<a href="/doLogout">LogOut</a>
<c:if test="${loggedIn}">
    <a href="/user">My profile</a>
</c:if>

    <table class="table">
        <c:forEach items="${products}" var="product">
            <tr>
                <td>${product.name}</td>
                <td>${product.genre}</td>
                <td>${product.price}</td>
                <td><c:if test="${loggedIn}">
                    <a class="btn btn-default" href="/user/wishlist/add/${product.id}" role="button">Add to wishlist</a>
                    <a class="btn btn-default" href="/user/cart/add/${product.id}" role="button">Add to my cart</a>
                </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
