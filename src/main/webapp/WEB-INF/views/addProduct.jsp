<%--
  Created by IntelliJ IDEA.
  User: felix
  Date: 6/18/19
  Time: 2:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <title>Add a new product</title>
</head>
<body>
<form:form modelAttribute="product">
    <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name:</label>
    </div>
    <div class="col-sm-10">
        <form:input path="name" cssClass="form-control"/>
    </div>
    <div class="form-group">
        <label for="genre" class="col-sm-2 control-label">Genre:</label>
    </div>
    <div class="col-sm-10">
        <form:input path="genre" cssClass="form-control"/>
    </div>
    <div class="form-group">
        <label for="price" class="col-sm-2 control-label">Price:</label>
    </div>
    <div class="col-sm-10">
        <form:input path="price" cssClass="form-control"/>
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form:form>
</body>
</html>
