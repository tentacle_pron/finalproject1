
<%@ page  language="java" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <title>Cart</title>
</head>
<body>

<table class="table">
    <c:forEach items="${products}" var="product">
    <tr>
        <td>${product.name}</td>
        <td>${product.genre}</td>
        <td>${product.price}</td>
    </tr>

    </c:forEach>

    <a href="/">Return to Homepage</a>
</body>
</html>
