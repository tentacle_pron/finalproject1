<%--
  Created by IntelliJ IDEA.
  User: felix
  Date: 6/16/19
  Time: 4:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" %>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<html>

<head>
    <%@ include file="taglib.jsp" %>
    <title>Registration page</title>
</head>
<body>
<h2 class="form-signin-heading">Register</h2>
<form:form modelAttribute="user" cssClass="form-horisontal">
<div class="form-group">
    <label for="username" class="col-sm-2 control-label">Username:</label>
</div>
    <div class="col-sm-10">
        <form:input path="username" cssClass="form-control"/>
    </div>
    <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Password:</label>
    </div>
    <div class="col-sm-10">
        <form:password path="password" cssClass="form-control"/>
    </div>
    <div class="form-group">
        <div class="col-sm-2">
            <input type="submit" value="Save" class="btn btn-lg btn-primary"/>
        </div>
    </div>
</form:form>
</body>
</html>
