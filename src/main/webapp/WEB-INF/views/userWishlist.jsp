<%@ include file="taglib.jsp"%>

<%@ page language="java" %>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<html>
<head>
    <title>My Wishlist</title>
</head>
<body>

<table class="table">
    <c:forEach items="${products}" var="product">
        <tr>
            <td>${product.name}</td>
            <td>${product.genre}</td>
            <td>${product.price}</td>
        </tr>
    </c:forEach>

    <a href="/">Return to Homepage</a>
</table>
</body>
</html>
