package main.java.database.services;

import main.java.database.entities.Product;

import java.util.List;

public interface ProductService {
    Product addProduct(Product product);
    void delete(long id);
    Product getByName(String name);
    Product getById(long id);
    Product editProduct(Product product);
    List<Product> getAll();
}
