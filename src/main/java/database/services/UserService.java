package main.java.database.services;

import main.java.database.entities.User;

import java.util.List;

public interface UserService {
    User addUser(User user);
    void delete(long id);
    User getByName(String name);
    User getById(long id);
    User editUser(User user);
    List<User> getAll();
}
