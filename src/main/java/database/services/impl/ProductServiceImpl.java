package main.java.database.services.impl;

import main.java.database.entities.Product;
import main.java.database.repository.ProductRepository;
import main.java.database.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Override
    public Product addProduct(Product product) {
        return repository.saveAndFlush(product);
    }

    @Override
    public void delete(long id) {
    repository.deleteById(id);
    }

    @Override
    public Product getByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Product getById(long id) {
        return repository.findById(id).orElse(new Product());
    }

    @Override
    public Product editProduct(Product product) {
        return repository.saveAndFlush(product);
    }

    @Override
    public List<Product> getAll() {
        return repository.findAll();
    }

}
