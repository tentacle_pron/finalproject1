package main.java.database.services.impl;

import main.java.database.entities.Cart;
import main.java.database.repository.CartRepository;
import main.java.database.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository repository;


    @Override
    public Cart addCart(Cart cart) {
        return repository.saveAndFlush(cart);
    }

    @Override
    public void delete(long id) {
    repository.deleteById(id);
    }

    @Override
    public Cart getById(long id) {
        return repository.findById(id).orElse(new Cart());
    }

    @Override
    public Cart editCart(Cart cart) {
        return repository.saveAndFlush(cart);
    }

    @Override
    public List<Cart> getAll() {
        return repository.findAll();
    }
}
