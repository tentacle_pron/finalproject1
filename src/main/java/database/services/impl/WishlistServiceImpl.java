package main.java.database.services.impl;

import main.java.database.entities.Wishlist;
import main.java.database.repository.WishlistRepository;
import main.java.database.services.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class WishlistServiceImpl implements WishlistService {

    @Autowired
    private WishlistRepository repository;

    @Override
    public Wishlist addWishlist(Wishlist wishlist) {
        return repository.saveAndFlush(wishlist);
    }

    @Override
    public void delete(long id) {
    repository.deleteById(id);
    }

    @Override
    public Wishlist getById(long id) {
        return repository.findById(id).orElse(new Wishlist());
    }

    @Override
    public Wishlist editWishlist(Wishlist wishlist) {
        return repository.saveAndFlush(wishlist);
    }

    @Override
    public List<Wishlist> getAll() {
        return repository.findAll();
    }
}
