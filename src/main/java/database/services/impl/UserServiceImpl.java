package main.java.database.services.impl;

import main.java.database.entities.User;
import main.java.database.repository.UserRepository;
import main.java.database.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public User addUser(User user) {
        return repository.saveAndFlush(user);
    }

    @Override
    public void delete(long id) {
    repository.deleteById(id);
    }

    @Override
    public User getByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public User getById(long id) {
        return repository.findById(id).orElse(new User());
    }

    @Override
    public User editUser(User user) {
        return repository.saveAndFlush(user);
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }


}
