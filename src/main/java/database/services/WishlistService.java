package main.java.database.services;


import main.java.database.entities.Wishlist;

import java.util.List;

public interface WishlistService {
        Wishlist addWishlist(Wishlist wishlist);
        void delete(long id);
        Wishlist getById(long id);
        Wishlist editWishlist(Wishlist wishlist);
        List<Wishlist> getAll();

}
