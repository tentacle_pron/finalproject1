package main.java.database.services;

import main.java.database.entities.Cart;

import java.util.List;

public interface CartService {
    Cart addCart(Cart cart);
    void delete(long id);
    Cart getById(long id);
    Cart editCart(Cart cart);
    List<Cart> getAll();
}
