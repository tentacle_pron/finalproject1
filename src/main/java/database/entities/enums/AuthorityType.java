package main.java.database.entities.enums;

public enum  AuthorityType {
    ROLE_ADMIN,
    ROLE_USER
}