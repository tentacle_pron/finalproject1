package main.java.controller;

import main.java.database.entities.User;
import main.java.database.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/user")
    public String openProfile(Model model, HttpSession session){
        System.out.println(session.getAttribute("userId"));
        long userId = Long.valueOf(session.getAttribute("userId").toString());
        User loggedUser =  userService.getById(userId);
        model.addAttribute("user", loggedUser);
        return "userProfile";
    }
}
