package main.java.controller;

import main.java.database.entities.Product;
import main.java.database.entities.User;
import main.java.database.entities.Wishlist;
import main.java.database.services.ProductService;
import main.java.database.services.UserService;
import main.java.database.services.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Collection;

@Controller
public class WishlistController {
    @Autowired
    private WishlistService wishlistService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;

    @RequestMapping("/user/wishlist/{id}")
    public String userWishlist(@PathVariable long id, Model model){
        Wishlist wishlist = wishlistService.getById(id);
        Collection<Product> products = wishlist.getProducts();
        model.addAttribute("products", products);
        return "userWishlist";
    }
    @RequestMapping("/user/wishlist/add/{id}")
    public String addToWishlist(@PathVariable long id, HttpSession session){
        User loggedUser = userService.getById((long) session.getAttribute("userId"));
        Wishlist wishlist = loggedUser.getWishlist();
        Collection<Product> products = wishlist.getProducts();
        products.add(productService.getById(id));
        wishlist.setProducts(products);
        wishlistService.editWishlist(wishlist);

        return "redirect:/";
    }
}
