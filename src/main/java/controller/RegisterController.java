package main.java.controller;

import main.java.database.entities.Cart;
import main.java.database.entities.User;
import main.java.database.entities.Wishlist;
import main.java.database.services.CartService;
import main.java.database.services.UserService;
import main.java.database.services.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterController {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;
    @Autowired
    private WishlistService wishlistService;
    @Autowired
    private CartService cartService;

    @ModelAttribute("user")
    public User construct(){
        return new User();
    }
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registrationPage(Model model){
        model.addAttribute("user", new User());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user){
        String s  = user.getPassword();
        user.setPassword(passwordEncoder.encode(s));
        Wishlist wishlist = new Wishlist();
        Cart cart = new Cart();
        wishlistService.addWishlist(wishlist);
        cartService.addCart(cart);
        user.setWishlist(wishlist);
        user.setCart(cart);
        userService.addUser(user);
        return "redirect:login";
    }
}
