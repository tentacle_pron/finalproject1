package main.java.controller;

import main.java.database.entities.Product;
import main.java.database.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdminController {
    @Autowired
    private ProductService productService;

    @RequestMapping("/admin/add")
    public String construct(Model model){
        model.addAttribute("product", new Product());

        return "addProduct";
    }
    @RequestMapping(value = "/admin/add", method = RequestMethod.POST)
    public String addProduct(@ModelAttribute Product product, Model model){
        productService.addProduct(product);


        return "redirect:/";
    }
}
