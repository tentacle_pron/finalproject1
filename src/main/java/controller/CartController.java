package main.java.controller;

import main.java.database.entities.Cart;
import main.java.database.entities.Product;
import main.java.database.entities.User;
import main.java.database.services.CartService;
import main.java.database.services.ProductService;
import main.java.database.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Collection;

@Controller
public class CartController {

    @Autowired
    private CartService cartService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;

    @RequestMapping("/user/cart/{id}")
    public String userCart(@PathVariable long id, Model model){
        Cart cart = cartService.getById(id);
        Collection<Product> products = cart.getProducts();

            model.addAttribute("products", products);



        return "userCart";
    }
    @RequestMapping("/user/cart/add/{id}")
    public String addToCart(@PathVariable long id, HttpSession session){
        User loggedUser = userService.getById(Long.valueOf(session.getAttribute("userId").toString()));
        Cart cart = loggedUser.getCart();
        Collection<Product> products = cart.getProducts();
        products.add(productService.getById(id));
        cart.setProducts(products);
        cartService.editCart(cart);

        return "redirect:/";
    }
}

