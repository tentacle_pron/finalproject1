package main.java.controller;

import main.java.database.entities.Product;
import main.java.database.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Collection;

@Controller
public class HomeController {
    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String homePage(Model model, HttpSession session){
        Collection<Product> products = productService.getAll();
        model.addAttribute("products", products);
        if(session.getAttribute("userId")!=null){
            model.addAttribute("loggedIn", true);
        }
        return "home";
    }
}
