package main.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class StoreRunner extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(StoreRunner.class, args);
    }
}
